﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Chessman {

    public King() {
        healthPoints = 2;
        damagePoints = 2;

        maxMovePoints = 2;
        movePoints = maxMovePoints;
        
        /*
        try {
            sprite = Resources.Load<GameObject>("Assets/Assets/Unit Models/White King.prefab");
        } catch {
            Debug.Log("Sprite does not exist.");
        };
        */
    }
}
