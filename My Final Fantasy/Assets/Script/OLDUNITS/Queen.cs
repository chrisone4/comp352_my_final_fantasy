﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : Chessman {

    Stack<int> xCoords = new Stack<int>();
    Stack<int> yCoords = new Stack<int>();
	
	public Queen() {
        healthPoints = 2;
        damagePoints = 1;
		attackPoints = 6;
		defensePoints = 4;

        maxMovePoints = 4;
        movePoints = maxMovePoints;
		
        /*
        try {
            sprite = Resources.Load<GameObject>("Assets/Assets/Unit Models/White King.prefab");
        } catch {
            Debug.Log("Sprite does not exist.");
        };
        */
    }

    public override bool[,] PossibleMove() {
        bool[,] r = new bool[8, 8];
        /*
        newUnitMove(CurrentX + 1, CurrentY, ref r); // up
        newUnitMove(CurrentX - 1, CurrentY, ref r); // down
        newUnitMove(CurrentX, CurrentY - 1, ref r); // left
        newUnitMove(CurrentX, CurrentY + 1, ref r); // right
        newUnitMove(CurrentX + 1, CurrentY - 1, ref r); // up left
        newUnitMove(CurrentX - 1, CurrentY - 1, ref r); // down left
        newUnitMove(CurrentX + 1, CurrentY + 1, ref r); // up right
        newUnitMove(CurrentX - 1, CurrentY + 1, ref r); // down right
        */

        //newUnitMove(destinationX, destinationY, ref r);
        someSortofSearch(destinationX, destinationY);
        while(xCoords.Count != 0) {
            newUnitMove(xCoords.Pop(), yCoords.Pop(), ref r);
        }

        return r;
    }

    public void newUnitMove(int x, int y, ref bool[,] r) {
        Chessman c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8) {
            c = BoardManager.Instance.Chessmans[x, y];
            if (c == null)
                r[x, y] = true;
            else if (isWhite != c.isWhite)
                r[x, y] = true;
        }
    }

    public void someSortofSearch(int x, int y) {
        Debug.Log(CurrentX != destinationX && y != destinationY);
        while (CurrentX != destinationX && y != destinationY) {
        //while (!(CurrentX != destinationX)) { 
            xCoords.Push(CurrentX);
            yCoords.Push(CurrentY);
            if (CurrentX < x) {
                CurrentX++;
            } else if(CurrentX > x) {
                CurrentX--;
            }
            
            if (CurrentY < y) {
                CurrentY++;
            } else if (CurrentY > y) {
                CurrentY++;
            }
        }
    }
}
