﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spearman : Chessman {
	
	public Spearman() {
        healthPoints = 2;
        damagePoints = 1;
		attackPoints = 6;
		defensePoints = 4;

        maxMovePoints = 3;
        movePoints = maxMovePoints;
        
        /*
        try {
            sprite = Resources.Load<GameObject>("Assets/Assets/Unit Models/White King.prefab");
        } catch {
            Debug.Log("Sprite does not exist.");
        };
        */
    }

	public override bool[,] PossibleMove(){
        bool[,] r = new bool[8, 8];

        UnitMove(CurrentX + 1, CurrentY, ref r); // up
        UnitMove(CurrentX - 1, CurrentY, ref r); // down
        UnitMove(CurrentX, CurrentY - 1, ref r); // left
        UnitMove(CurrentX, CurrentY + 1, ref r); // right
        UnitMove(CurrentX + 1, CurrentY - 1, ref r); // up left
        UnitMove(CurrentX - 1, CurrentY - 1, ref r); // down left
        UnitMove(CurrentX + 1, CurrentY + 1, ref r); // up right
        UnitMove(CurrentX - 1, CurrentY + 1, ref r); // down right
		
		SpearAttack(CurrentX, CurrentY+2, ref r);

        return r;
    }
	
    public void SpearAttack(int x, int y, ref bool[,] r) {
        Chessman c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8) {
            c = BoardManager.Instance.Chessmans[x, y];
            if (c != null && (isWhite != c.isWhite))
                r[x, y] = true;
        }
    }
}
