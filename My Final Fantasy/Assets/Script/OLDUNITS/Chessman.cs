﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Courtesy of https://www.youtube.com/watch?v=on_J2IFoPME&ab_channel=N3KEN

public abstract class Chessman : MonoBehaviour {

    public GameObject Unit;

    private List<GameObject> unitPrefabs;
    private List<GameObject> activeUnit;

    public GameObject sprite;

    private Quaternion orientation = Quaternion.Euler(0, 90, 0);

    public int CurrentX { set; get; }
    public int CurrentY { set; get; }
    public int InitialX;
    public int InitialY;
    public bool isWhite;

    public int tempHP;
    public int tempDAM;
    public int tempAttack;
    public int tempDefense;

    public int tempMaxMovePoints;
    public int tempMovePoints;

    protected int destinationX = -1;
    protected int destinationY = -1;

    //private int healthPoints { set; get; }
    //private int damagePoints { set; get; }

    protected int healthPoints;
    protected int damagePoints;
	protected int attackPoints;
	protected int defensePoints;
	
    protected string pieceName;
    public string unitName;

    protected int maxMovePoints;
    protected int movePoints;

    public string getName() {
        return pieceName;
    }

    public string setName(string newName) {
        return pieceName = newName;
    }

    public int getHealth() {
        return healthPoints;
    }

    public void setHealth(int newHealth) {
        healthPoints = newHealth;
    }

    public int getDamage() {
        return damagePoints;
    }

    public void setDamage(int newDamage) {
        damagePoints = newDamage;
    }
    
    public int getMaxMovePoints() {
        return maxMovePoints;
    }

    public int getMovePoints() {
        return movePoints;
    }

    public void setMovePoints(int newMovePoints) {
		movePoints = newMovePoints;
    }
	
	public int getAttack(){
		return attackPoints;
	}
	
	public int getDefense(){
		return defensePoints;
	}
    
    public void SetPosition(int x, int y){
		CurrentX = x;
        CurrentY = y;
	}

    public int getCurrentX() {
        return CurrentX;
    }
	
	public virtual bool[,] PossibleMove(){
        bool[,] r = new bool[8, 8];

        UnitMove(CurrentX + 1, CurrentY, ref r); // up
        UnitMove(CurrentX - 1, CurrentY, ref r); // down
        UnitMove(CurrentX, CurrentY - 1, ref r); // left
        UnitMove(CurrentX, CurrentY + 1, ref r); // right
        UnitMove(CurrentX + 1, CurrentY - 1, ref r); // up left
        UnitMove(CurrentX - 1, CurrentY - 1, ref r); // down left
        UnitMove(CurrentX + 1, CurrentY + 1, ref r); // up right
        UnitMove(CurrentX - 1, CurrentY + 1, ref r); // down right

        return r;
    }

    public void UnitMove(int x, int y, ref bool[,] r) {
        Chessman c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8) {
            c = BoardManager.Instance.Chessmans[x, y];
            if (c == null)
                r[x, y] = true;
            else if (isWhite != c.isWhite)
                r[x, y] = true;
        }
    }
    
    public void SpawnUnit(int index, int x, int y) {
        unitPrefabs = GetComponent<BoardManager>().chessmanPrefabs;
        activeUnit = GetComponent<BoardManager>().getactiveUnit();
        Debug.Log("HELLO");
        Vector3 tileCenter = GetComponent<BoardManager>().getGetTileCenter(x, y);
        Chessman[,] Units = GetComponent<BoardManager>().Chessmans;

        GameObject go = Instantiate(unitPrefabs[index], tileCenter, orientation) as GameObject;

        go.transform.Translate(0, 1, 0);
        go.transform.Rotate(90, 0, 0);

        go.transform.SetParent(transform);
        Units[x, y] = go.GetComponent<Chessman>();
        Units[x, y].SetPosition(x, y);
        activeUnit.Add(go);
    }
    
    public void Destination() {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane"))) {
            destinationX = (int)hit.point.x;
            destinationY = (int)hit.point.z;
        }
        else {
            destinationX = -1;
            destinationY = -1;
        }
    }
}
