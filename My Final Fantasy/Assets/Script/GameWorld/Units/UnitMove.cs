﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Courtesy of: https://www.youtube.com/watch?v=mTNL0EXU7kU&t=2259s&ab_channel=GameProgrammingAcademy

public abstract class UnitMove : TacticsMove {

    void Start() {
        Init();

        GetCurrentTile();
    }

    void Update() {
        if (!turn) {
            return;
        }

        if (!moving) {
            FindSelectableTiles();
            CheckMouse();
        } else {
            Move();
        }
    }

    void CheckMouse() {
        if (Input.GetMouseButtonUp(1)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)) {
                if(hit.collider.tag == "Tile") {
                    Tile t = hit.collider.GetComponent<Tile>();

                    if (t.selectable) {
                        MoveToTile(t);
                    }

                    if (t.attackable) {
<<<<<<< HEAD
                       combatManager.Combat(t);
=======
                       combatManager.mainCombat(t);
>>>>>>> 60e3143437d4ba74c5fd7761090fd4437b5a9b9a
                    }
                }
            }
        }
    }
}
