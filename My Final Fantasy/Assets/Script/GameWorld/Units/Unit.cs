﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : UnitMove {

    public GameObject UnitModel;

    public int CurrentX { set; get; }
    public int CurrentY { set; get; }
    //public bool isPlayer;

    public Unit getUnit() {
        return this;
    }

    public void SetPosition(int x, int y) {
        CurrentX = x;
        CurrentY = y;
    }
}
