﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitTest : Unit {

    public UnitTest() {
        move = 5;
        moveSpeed = 2;
        maxMove = move;

        damagePoints = 1;
        attackPoints = 4;
        defensePoints = 2;
        healthPoints = 3;
    }
}
