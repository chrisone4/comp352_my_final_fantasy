﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESpear : NPCMove {

    public ESpear() {
        move = 3;
        moveSpeed = 2;
        maxMove = move;

        attackPoints = 5;
        defensePoints = 5;
        damagePoints = 5;

        healthPoints = 10;

        attackRange = 2;
        isSpear = true;

        //ability = SpecialAbility.getAbility();
    }
}
