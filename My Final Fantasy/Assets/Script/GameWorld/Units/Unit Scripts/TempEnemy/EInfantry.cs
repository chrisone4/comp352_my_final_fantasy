﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EInfantry : NPCMove {

    public EInfantry() {
        unitName = "Infantry";

        move = 3;
        moveSpeed = 2;
        maxMove = move;

        damagePoints = 5;
        attackPoints = 7;
        defensePoints = 7;

        healthPoints = 12;

        attackRange = 1;
        isMelee = true;

        //ability = SpecialAbility.getAbility();
    }
}
