﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Courtesy of https://www.youtube.com/watch?v=cK2wzBCh9cg&ab_channel=GameProgrammingAcademy

/* Combat Update
 * Seemingly Impossible to do with Checking Colliders
 * 
 * Try xCoords/yCoords like previous program
 * Where each piece somehow tells the Tile it is standing on that the unit now occupies said tile.
 * 
 */ 

public class Tile : MonoBehaviour {

    public bool walkable = true;

    public bool current = false;
    public bool target = false;
    public bool confirmTarget = false;
    public bool selectable = false;

    public bool attackable = false;
	public bool attackRange = false;

    public List<Tile> adjacencyList = new List<Tile>();

    public int xCoord;
    public int yCoord;

    public TacticsMove unit;

    //BFS
    public bool visited = false;
    public Tile parent = null;
    public int distance = 0;

    //A*
    public float f = 0;
    public float g = 0;
    public float h = 0;
	
	public bool attackVisit = false;

    // Use this for initialization
    void Start () {

	}

    // Update is called once per frame
    void Update () {
        if (current) {
            GetComponent<Renderer>().material.color = Color.magenta;
        } else if (target) {
            GetComponent<Renderer>().material.color = Color.green;
        } else if (selectable) {
            GetComponent<Renderer>().material.color = Color.yellow;
        } else if (attackable) {
            GetComponent<Renderer>().material.color = Color.red;
		} else if (attackRange) {
			GetComponent<Renderer>().material.color = Color.grey;
        } else {
            GetComponent<Renderer>().material.color = Color.white;
        }
	}

    public void Reset() {
        adjacencyList.Clear();
        current = false;
        target = false;
        confirmTarget = false;
        selectable = false;
		
        attackable = false;
		attackRange = false;
        
        visited = false;
		attackVisit = false;
        parent = null;
        distance = 0;

        f = g = h = 0;
    }

    public void findNeighbors(Tile target, bool attacking) {
        Reset();
        
        CheckTile3D(Vector3.forward, target, attacking);
        CheckTile3D(-Vector3.forward, target, attacking);
        CheckTile3D(Vector3.right, target, attacking);
        CheckTile3D(-Vector3.right, target, attacking);
		
		/*
		CheckTile(Vector3.forward, target);
        CheckTile(-Vector3.forward, target);
        CheckTile(Vector3.right, target);
        CheckTile(-Vector3.right, target);
		*/
    }
    
    public void CheckTile3D(Vector3 direction, Tile target, bool attacking) {
        Vector3 halfExtends = new Vector3(0.25f, 0.25f, 0.25f);
        int Player_Layer = ~(1 << 10);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtends, Quaternion.identity, Player_Layer);
        //Debug.Log(transform.position + direction);
        //Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtends);
        
        if (!attacking) { // If player is moving
            foreach (Collider item in colliders) {
                Tile tile = item.GetComponent<Tile>();
                if (tile != null) {
                    RaycastHit hit;
                    if (!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1) || (tile == target)) {
                        adjacencyList.Add(tile);
                    }
                }
            }
        } else { // If player is attacking
            foreach (Collider item in colliders) {
                Tile tile = item.GetComponent<Tile>();
                //if (tile != null && tile.walkable) {
                if (tile != null) {
                    RaycastHit hit;
                    if (tile.unit != null) {
                        adjacencyList.Add(tile);
                        tile.walkable = false;
					} else if (!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1) || (tile == target)) {
						adjacencyList.Add(tile);
					}
                }
            }
        }
    }
    
    
    public void CheckTile(Vector3 direction, Tile target) {
        Vector3 halfExtends = new Vector3(0.25f, 0.25f, 0.25f);
        int NPC_Layer = ~(1 << 11);
        int Player_Layer = ~(1 << 10);
        //NEED TO CHANGE FOR 2D
        //Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtends);
        Collider[] colliders = Physics.OverlapBox(transform.position + direction, halfExtends, Quaternion.identity, Player_Layer);

        //if (Physics.Raycast(transform.position, direction, 1, NPC_Layer)) {
        //if (Physics.CheckBox(transform.position + direction, halfExtends, Quaternion.identity, NPC_Layer)) {
        //    tile.attackable = true;
        //}

        foreach (Collider item in colliders) {
            Tile tile = item.GetComponent<Tile>();
            //if (tile != null && tile.walkable) {
            if (tile != null) {
                RaycastHit hit;
                if (tile.unit != null){
                    adjacencyList.Add(tile);
                    tile.walkable = false;
                } else if(!Physics.Raycast(tile.transform.position, Vector3.up, out hit, 1) || (tile == target)) {
                    adjacencyList.Add(tile);
                }
            }
        }
    }

    /* foreach (Collider item in colliders) {
                Tile tile = item.GetComponent<Tile>();
                //if (tile != null && tile.walkable) {
                if (tile != null) {
                    RaycastHit hit;
                    if (tile.unit != null) {
                        adjacencyList.Add(tile);
                        tile.walkable = false;
                    }
                }
            }
    */
}
